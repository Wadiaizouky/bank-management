﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class Deposit : Form
    {
        public Deposit()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            Class1 c = new Class1();
            if (textBox2.Text != "")
            {
                label2.Text = " الرصيد الكلي" + "   " + c.select(textBox2.Text);
            }
            else
                MessageBox.Show("الرجاء ادخال الاسم");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" & textBox2.Text == "")
                MessageBox.Show("الرجاء ادخال الاسم والكمية المراد اضافتها الى الحساب");
            else
                if (textBox1.Text != "" & textBox2.Text == "")
                    MessageBox.Show("الرجاء ادخال الاسم");
            else
                    if (textBox1.Text == "" & textBox2.Text != "")
                        MessageBox.Show("الرجاء ادخال الكمية المراد اضافتها الى الحساب");
                    else
                    {
                        float amount = float.Parse(textBox1.Text);
                        Class1 c = new Class1();
                        float money = c.select(textBox2.Text);
                        float newmoney = money + amount;
                        c.update(money,newmoney);
                        MessageBox.Show("تمت اضافة" + " " + amount + " " + "الى رصيدك");
                        label2.Text = " الرصيد الكلي" + "   " + c.select(textBox2.Text);
                    }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            if (textBox2.Text != "")
            {
                MessageBox.Show(" رصيد  " + textBox2.Text + "  هو" + c.select(textBox2.Text));
            }
            else
                MessageBox.Show("الرجاء ادخال الاسم");
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" & textBox2.Text == "")
                MessageBox.Show("الرجاء ادخال الاسم والكمية المراد اضافتها الى الحساب");
            else
                if (textBox1.Text != "" & textBox2.Text == "")
                    MessageBox.Show("الرجاء ادخال الاسم");
                else
                    if (textBox1.Text == "" & textBox2.Text != "")
                        MessageBox.Show("الرجاء ادخال الكمية المراد اضافتها الى الحساب");
                    else
                    {
                        float amount = float.Parse(textBox1.Text);
                        Class1 c = new Class1();
                        float money = c.select(textBox2.Text);
                        float newmoney = money + amount;
                        c.update(money, newmoney);
                        MessageBox.Show("تمت اضافة" + " " + amount + " " + "الى رصيدك");
                        label2.Text = " الرصيد الكلي" + "   " + c.select(textBox2.Text);
                    }
        }

        private void Deposit_Load(object sender, EventArgs e)
        {
            ToolTip t = new ToolTip();
            t.ShowAlways = true;
            t.SetToolTip(pictureBox4, "الاستعلام عن رصيد");
            t.SetToolTip(pictureBox5, "موافق");
        }
    }
}
