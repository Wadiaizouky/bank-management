﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class CustomerMang : Form
    {
        public CustomerMang()
        {
            InitializeComponent();
        }

        private void CustomerMang_Load(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            dataGridView1.DataSource = c.selectAllCustomer();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            CreateAccount w = new CreateAccount();
            w.Visible = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            int id = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            c.DeleteUser(id);
            MessageBox.Show("تم الحذف بنجاح");
            dataGridView1.DataSource = c.selectAllCustomer();
        }
    }
}
