﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class Mangement : Form
    {
        public Mangement()
        {

            InitializeComponent();          
            
        }

        private void Mangement_Load(object sender, EventArgs e)
        {
            ToolTip t = new ToolTip();
            t.ShowAlways = true;
            t.SetToolTip(pictureBox1, "جديد");
            t.SetToolTip(pictureBox2, "إضافة");
            t.SetToolTip(pictureBox3, "حذف");
            t.SetToolTip(pictureBox4, "تعديل");
            Class1 c = new Class1();
            dataGridView1.DataSource = c.selectAllUser();

            //textBox1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            //textBox2.Text = Convert.ToInt32(dataGridView1.CurrentRow.Cells[3].Value).ToString();
            //comboBox1.SelectedItem = dataGridView1.CurrentRow.Cells[1].Value.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            pictureBox2.Enabled = true;
            pictureBox1.Enabled = false;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string username = textBox1.Text;
            string pass = textBox2.Text;
            string Type = comboBox1.SelectedItem.ToString();
            c.insertUser(Type,username,pass);
            MessageBox.Show("تمت الإضافة بنجاح");
            dataGridView1.DataSource = c.selectAllUser();
            pictureBox2.Enabled = false;
            pictureBox1.Enabled = true;
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            int id = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            c.DeleteUser(id);
            MessageBox.Show("تم الحذف بنجاح");
            dataGridView1.DataSource = c.selectAllUser();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            int id = Int32.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            string username = textBox1.Text;
            string pass = textBox2.Text;
            string Type = comboBox1.SelectedItem.ToString();
            c.UpdateUser(id,Type, username, pass);
            MessageBox.Show("تم التعديل بنجاح");
            dataGridView1.DataSource = c.selectAllUser();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
