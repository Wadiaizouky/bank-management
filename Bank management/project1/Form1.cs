﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class Form1 : Form
    {
        private static Form1 sh;
        public Form1()
        {
            if (sh == null)
                sh = this;
            InitializeComponent();
        }
        static void FRM_PRODUCTS_Close(object sender, FormClosedEventArgs e)
        {
            sh = null;
        }
        public static Form1 get
        {
            get
            {
                if (sh == null)
                {
                    sh = new Form1();
                    sh.FormClosed += new FormClosedEventHandler(FRM_PRODUCTS_Close);
                }
                return sh;
            }
        }

        private void سحبمالToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            withdraw w = new withdraw();
            w.Visible = true;
            //this.Close();

        }

        private void ايداعمالToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Deposit w = new Deposit();
            w.Visible = true;
        }

        private void سحبقرضToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Loan w = new Loan();
            w.Visible = true;
        }

        private void تسجيلالخروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.get.إنشاءحسابToolStripMenuItem.Enabled = false;
            Form1.get.سحبمالToolStripMenuItem.Enabled = false;
            Form1.get.ايداعمالToolStripMenuItem.Enabled = false;
            Form1.get.سحبقرضToolStripMenuItem.Enabled = false;
            Form1.get.اداروالعملاءوالمستخدمينToolStripMenuItem.Enabled = false;
            Form1.get.تحويلمالمنحسابلاخرToolStripMenuItem.Enabled = false;
        }

        private void تسجيلالدخولToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            Log_in w = new Log_in();
            w.Visible = true;
        }

        private void اداروالعملاءوالمستخدمينToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void إنشاءحسابToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void فتححسابلشخصواحدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateAccount w = new CreateAccount();
            w.Visible = true;
        }

        private void فتححسابمشتركToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateAccount2 w = new CreateAccount2();
            w.Visible = true;
        }

        private void حولالبرنامجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("بتصميم برنامج لادارة البنوك الذي سيستخدم في العديد من البنوك التجارية المحلية والعالمية.");
        }

        private void تسجيلالدخولToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void تحويلمالمنحسابلاخرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TransferMoney w = new TransferMoney();
            w.Visible = true;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ادارةالمستخدمينToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mangement w = new Mangement();
            w.Visible = true;
        }

        private void ادارةالعملاءToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerMang w = new CustomerMang();
            w.Visible = true;
        }
    }
}
