﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace project1
{
    class Class1
    {
        SqlConnection sqlconnection ;
        SqlDataReader sqlr;
        public Class1() {
            sqlconnection = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\wade\Desktop\project1\project1\project1\Database1.mdf;Integrated Security=True;User Instance=True");
            
        }

        public void Open()
        {
            if (sqlconnection.State != ConnectionState.Open)
                sqlconnection.Open();
        }
        //Method to close the connection
        public void Close()
        {
            if (sqlconnection.State != ConnectionState.Closed)
                sqlconnection.Close();
        }

        public void update(float oldmoney,float newmoney) {

            Open();
            sqlr.Close();
            string commend = "update Customer set Money =" + newmoney + " where Money =" + oldmoney;
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
          
        }

        public float select(string name)
        {
            Open(); 
            float result = 0;
            DataTable dt = new DataTable();
            string commend = "select Money from Customer where Name ='" + name + "' OR Name2 = '"+name+"'";//sql
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlr = sqlcommend.ExecuteReader();
            while (sqlr.Read()) {
                result = float.Parse(sqlr[0].ToString());
            }
            return result;
            
            Close();
        }

        public void insert(string name, int benefit, int Straight, int Value, int Length)
        {
            Open();
            //sqlr.Close();
            string commend = "insert into Give(Name,benefit,Straight,Value,Length) values ('" + name + "'," + benefit + "," + Straight+"," + Value +","+ Length+")";
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public string selectuser(string name,string pass)
        {
            Open();
            string result = "";
            DataTable dt = new DataTable();
            string commend = "select Type from Users where Username ='" + name + "'"+"and Pass ='"+pass+"'";
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlr = sqlcommend.ExecuteReader();
            while (sqlr.Read())
            {
                result = sqlr[0].ToString();
            }
            return result;

            Close();
        }

        public void insertCustomer(string name, string FatherName, string City, string Title, float Money, int Age, int Benefit)
        {
            Open();
            //sqlr.Close();
            string commend = "insert into Customer(Name,FatherName,City,Title,Money,Age,Benefit) values ('" + name + "','" + FatherName + "','" + City + "','" + Title + "'," + Money + "," + Age + "," + Benefit + ")";
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public void insertCustomer2(string name, string FatherName, string City, string Title, int Age, string name2, string FatherName2, string City2, string Title2,int Age2, float Money, int Benefit)
        {
            Open();
            //sqlr.Close();
            string commend = "insert into Customer(Name,FatherName,City,Title,Age,Name2,FatherName2,City2,Title2,Age2,Money,Benefit) values ('" + name + "','" + FatherName + "','" + City + "','" + Title + "'," + Age +",'" + name2 + "','" + FatherName2 + "','" + City2 + "','" + Title2 + "'," + Age2+","+Money+","+Benefit+")";
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public DataTable selectAllUser()
        {
            Open();
            string result = "";
            DataTable dt = new DataTable();
            string commend = "select * from Users";
            SqlDataAdapter sqd = new SqlDataAdapter(commend,sqlconnection);
            sqd.Fill(dt);
            return dt;
            Close();
        }

        public void insertUser(string Type, string Username, string pass)
        {
            Open();
            //sqlr.Close();
            string commend = "insert into Users(Type,Username,Pass) values ('"+Type+"','"+Username+"','"+pass+"')";
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public void DeleteUser(int Id)
        {
            Open();
            //sqlr.Close();
            string commend = "delete from Customer where Id =" + Id;
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public void UpdateUser(int Id, string Type, string Username, string pass)
        {
            Open();
            //sqlr.Close();
            string commend = "update Users set Type ='" + Type + "',Username='" + Username + "',pass='" + pass + "' where Id =" + Id;
            SqlCommand sqlcommend = new SqlCommand(commend, sqlconnection);
            sqlcommend.ExecuteNonQuery();
            Close();
        }

        public DataTable selectAllCustomer()
        {
            Open();
            string result = "";
            DataTable dt = new DataTable();
            string commend = "select * from Customer";
            SqlDataAdapter sqd = new SqlDataAdapter(commend, sqlconnection);
            sqd.Fill(dt);
            return dt;
            Close();
        }
    }
}
