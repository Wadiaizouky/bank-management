﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class Loan : Form
    {
        public Loan()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("خطأ");
            }
            else
            {
                Class1 c = new Class1();
                string name = textBox3.Text;
                int t = int.Parse(textBox1.Text);
                int m = int.Parse(textBox2.Text);
                int s = t + t * 10 * m / 100;
                int mm = m * 12;
                int r = s / mm;
                c.insert(name, s, r, t, m);
                label6.Text = s.ToString();
                label7.Text = r.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("خطأ");
            }
            else
            {
                Class1 c = new Class1();
                string name = textBox3.Text;
                int t = int.Parse(textBox1.Text);//المبلغ
                int m = int.Parse(textBox2.Text);//المدة
                int mm = m * 12;//المدة الشهرية 
                int a = t * 10 / 100;//الفائدة السنوية
                int month = a/12;//الفائدة الشهرية
                int aa = a * m;//الفئدة الكلية
                int aaa = aa + t;//المبلغ الكلي الواجب دفعه
                int s = aaa / mm;//القسط الشهري
                c.insert(name, aa, s, t, m);
                label12.Text = mm.ToString();
                label14.Text = month.ToString();
                label6.Text = aa.ToString();
                label7.Text = s.ToString();
            }
        }
    }
}
