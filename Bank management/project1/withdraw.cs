﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class withdraw : Form
    {
        public withdraw()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string name = textBox2.Text;
            float intermoney = float.Parse(textBox1.Text);
            float oldmoney = c.select(name);
            float res = 0;
            if (oldmoney > intermoney)
            {
                res = oldmoney - intermoney;
                c.update(oldmoney, res);
                MessageBox.Show("تمت العملية بنجاح");
            }
            else
                MessageBox.Show("الرجاء ادخل بيانات صحيحة");

            
            float money = c.select(name);
            label6.Text = intermoney.ToString();
            label5.Text = money.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            if (textBox2.Text != "")
            {
                float f = c.select(textBox2.Text);
                if (f != 0)
                    MessageBox.Show(" رصيد"+textBox2.Text+"هو  "+f);
                else
                    MessageBox.Show("المستخدم غير موجود !!");
            }
            else
                MessageBox.Show("الرجاء ادخال الاسم");
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            if (textBox2.Text != "")
            {
                float f = c.select(textBox2.Text);
                if (f != 0)
                    MessageBox.Show(" رصيد  " + textBox2.Text + "  هو" + f);
                else
                    MessageBox.Show("المستخدم غير موجود !!");
            }
            else
                MessageBox.Show("الرجاء ادخال الاسم");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string name = textBox2.Text;
            float intermoney = float.Parse(textBox1.Text);
            float oldmoney = c.select(name);
            float res = 0;
            if (oldmoney > intermoney)
            {
                res = oldmoney - intermoney;
                c.update(oldmoney, res);
                MessageBox.Show("تمت العملية بنجاح");
            }
            else
                MessageBox.Show("الرجاء ادخل بيانات صحيحة");


            float money = c.select(name);
            label6.Text = intermoney.ToString();
            label5.Text = money.ToString();
        }

        private void withdraw_Load(object sender, EventArgs e)
        {
            ToolTip t = new ToolTip();
            t.ShowAlways = true;
            t.SetToolTip(pictureBox1, "الاستعلام عن رصيد");
            t.SetToolTip(pictureBox3, "موافق");
        }
    }
}
