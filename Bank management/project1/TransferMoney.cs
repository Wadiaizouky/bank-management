﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class TransferMoney : Form
    {
        public TransferMoney()
        {
            InitializeComponent();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string name = textBox1.Text;
            float intermoney = float.Parse(textBox3.Text);
            float oldmoney = c.select(name);
            float res = 0;
            if (oldmoney > intermoney)
            {
                res = oldmoney - intermoney;
                c.update(oldmoney, res);

                float money = c.select(textBox2.Text);
                float newmoney = money + intermoney;
                c.update(money, newmoney);
                MessageBox.Show("تمت العملية بنجاح");
            }
            else
                MessageBox.Show("الرجاء ادخل بيانات صحيحة");


        }
    }
}
