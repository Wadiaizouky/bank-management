﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace project1
{
    public partial class Log_in : Form
    {
        public Log_in()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string name = textBox1.Text;
            string pass = textBox2.Text;
            string type;
            if (name != "" && pass != "") {
                type = c.selectuser(name ,pass);
                if (type != "")
                {
                    if (type == "admin")
                    {
                        Form1 f = new Form1();
                        Form1.get.اداروالعملاءوالمستخدمينToolStripMenuItem.Enabled = true;
                        this.Close();
                    }
                    else {
                        Form1.get.إنشاءحسابToolStripMenuItem.Enabled = true;
                        Form1.get.سحبمالToolStripMenuItem.Enabled = true;
                        Form1.get.ايداعمالToolStripMenuItem.Enabled = true;
                        Form1.get.سحبقرضToolStripMenuItem.Enabled = true;
                        this.Close();
                    }
                }
                else
                    MessageBox.Show("المستخدم غير موجود");
            }
            else
                MessageBox.Show("الرجاء ادخال البيانات !!");
            
            
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Class1 c = new Class1();
            string name = textBox1.Text;
            string pass = textBox2.Text;
            string type;
            if (name != "" && pass != "")
            {
                type = c.selectuser(name, pass);
                if (type != "")
                {
                    if (type == "admin")
                    {
                        Form1 f = new Form1();
                        Form1.get.اداروالعملاءوالمستخدمينToolStripMenuItem.Enabled = true;
                        this.Close();
                    }
                    else
                    {
                        Form1.get.إنشاءحسابToolStripMenuItem.Enabled = true;
                        Form1.get.سحبمالToolStripMenuItem.Enabled = true;
                        Form1.get.ايداعمالToolStripMenuItem.Enabled = true;
                        Form1.get.سحبقرضToolStripMenuItem.Enabled = true;
                        Form1.get.تحويلمالمنحسابلاخرToolStripMenuItem.Enabled = true;
                        this.Close();
                    }
                }
                else
                    MessageBox.Show("المستخدم غير موجود");
            }
            else
                MessageBox.Show("الرجاء ادخال البيانات !!");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
