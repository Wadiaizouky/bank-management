﻿namespace project1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.تسجيلالدخولToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلالدخولToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.حولالبرنامجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلالخروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إنشاءحسابToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.فتححسابلشخصواحدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.فتححسابمشتركToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.سحبمالToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ايداعمالToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تحويلمالمنحسابلاخرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.سحبقرضToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اداروالعملاءوالمستخدمينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ادارةالعملاءToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ادارةالمستخدمينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(142, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(438, 294);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // تسجيلالدخولToolStripMenuItem
            // 
            this.تسجيلالدخولToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.تسجيلالدخولToolStripMenuItem1,
            this.حولالبرنامجToolStripMenuItem,
            this.تسجيلالخروجToolStripMenuItem});
            this.تسجيلالدخولToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.تسجيلالدخولToolStripMenuItem.Name = "تسجيلالدخولToolStripMenuItem";
            this.تسجيلالدخولToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.تسجيلالدخولToolStripMenuItem.Text = "ملف";
            this.تسجيلالدخولToolStripMenuItem.Click += new System.EventHandler(this.تسجيلالدخولToolStripMenuItem_Click);
            // 
            // تسجيلالدخولToolStripMenuItem1
            // 
            this.تسجيلالدخولToolStripMenuItem1.Name = "تسجيلالدخولToolStripMenuItem1";
            this.تسجيلالدخولToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
            this.تسجيلالدخولToolStripMenuItem1.Text = "تسجيل الدخول";
            this.تسجيلالدخولToolStripMenuItem1.Click += new System.EventHandler(this.تسجيلالدخولToolStripMenuItem1_Click);
            // 
            // حولالبرنامجToolStripMenuItem
            // 
            this.حولالبرنامجToolStripMenuItem.Name = "حولالبرنامجToolStripMenuItem";
            this.حولالبرنامجToolStripMenuItem.Size = new System.Drawing.Size(172, 26);
            this.حولالبرنامجToolStripMenuItem.Text = "حول البرنامج";
            this.حولالبرنامجToolStripMenuItem.Click += new System.EventHandler(this.حولالبرنامجToolStripMenuItem_Click);
            // 
            // تسجيلالخروجToolStripMenuItem
            // 
            this.تسجيلالخروجToolStripMenuItem.Name = "تسجيلالخروجToolStripMenuItem";
            this.تسجيلالخروجToolStripMenuItem.Size = new System.Drawing.Size(172, 26);
            this.تسجيلالخروجToolStripMenuItem.Text = "تسجيل الخروج";
            this.تسجيلالخروجToolStripMenuItem.Click += new System.EventHandler(this.تسجيلالخروجToolStripMenuItem_Click);
            // 
            // إنشاءحسابToolStripMenuItem
            // 
            this.إنشاءحسابToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.فتححسابلشخصواحدToolStripMenuItem,
            this.فتححسابمشتركToolStripMenuItem});
            this.إنشاءحسابToolStripMenuItem.Enabled = false;
            this.إنشاءحسابToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.إنشاءحسابToolStripMenuItem.Name = "إنشاءحسابToolStripMenuItem";
            this.إنشاءحسابToolStripMenuItem.Size = new System.Drawing.Size(93, 25);
            this.إنشاءحسابToolStripMenuItem.Text = "فتح حساب ";
            this.إنشاءحسابToolStripMenuItem.Click += new System.EventHandler(this.إنشاءحسابToolStripMenuItem_Click);
            // 
            // فتححسابلشخصواحدToolStripMenuItem
            // 
            this.فتححسابلشخصواحدToolStripMenuItem.Name = "فتححسابلشخصواحدToolStripMenuItem";
            this.فتححسابلشخصواحدToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.فتححسابلشخصواحدToolStripMenuItem.Text = "فتح حساب لشخص واحد";
            this.فتححسابلشخصواحدToolStripMenuItem.Click += new System.EventHandler(this.فتححسابلشخصواحدToolStripMenuItem_Click);
            // 
            // فتححسابمشتركToolStripMenuItem
            // 
            this.فتححسابمشتركToolStripMenuItem.Name = "فتححسابمشتركToolStripMenuItem";
            this.فتححسابمشتركToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.فتححسابمشتركToolStripMenuItem.Text = "فتح حساب مشترك";
            this.فتححسابمشتركToolStripMenuItem.Click += new System.EventHandler(this.فتححسابمشتركToolStripMenuItem_Click);
            // 
            // سحبمالToolStripMenuItem
            // 
            this.سحبمالToolStripMenuItem.Enabled = false;
            this.سحبمالToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.سحبمالToolStripMenuItem.Name = "سحبمالToolStripMenuItem";
            this.سحبمالToolStripMenuItem.Size = new System.Drawing.Size(87, 25);
            this.سحبمالToolStripMenuItem.Text = "سحب مال";
            this.سحبمالToolStripMenuItem.Click += new System.EventHandler(this.سحبمالToolStripMenuItem_Click);
            // 
            // ايداعمالToolStripMenuItem
            // 
            this.ايداعمالToolStripMenuItem.Enabled = false;
            this.ايداعمالToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ايداعمالToolStripMenuItem.Name = "ايداعمالToolStripMenuItem";
            this.ايداعمالToolStripMenuItem.Size = new System.Drawing.Size(79, 25);
            this.ايداعمالToolStripMenuItem.Text = "ايداع مال";
            this.ايداعمالToolStripMenuItem.Click += new System.EventHandler(this.ايداعمالToolStripMenuItem_Click);
            // 
            // تحويلمالمنحسابلاخرToolStripMenuItem
            // 
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Enabled = false;
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Name = "تحويلمالمنحسابلاخرToolStripMenuItem";
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Size = new System.Drawing.Size(187, 25);
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Text = "تحويل مال من حساب لاخر";
            this.تحويلمالمنحسابلاخرToolStripMenuItem.Click += new System.EventHandler(this.تحويلمالمنحسابلاخرToolStripMenuItem_Click);
            // 
            // سحبقرضToolStripMenuItem
            // 
            this.سحبقرضToolStripMenuItem.Enabled = false;
            this.سحبقرضToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.سحبقرضToolStripMenuItem.Name = "سحبقرضToolStripMenuItem";
            this.سحبقرضToolStripMenuItem.Size = new System.Drawing.Size(98, 25);
            this.سحبقرضToolStripMenuItem.Text = "سحب قرض";
            this.سحبقرضToolStripMenuItem.Click += new System.EventHandler(this.سحبقرضToolStripMenuItem_Click);
            // 
            // اداروالعملاءوالمستخدمينToolStripMenuItem
            // 
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ادارةالعملاءToolStripMenuItem,
            this.ادارةالمستخدمينToolStripMenuItem});
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Enabled = false;
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Name = "اداروالعملاءوالمستخدمينToolStripMenuItem";
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Text = "ادارة";
            this.اداروالعملاءوالمستخدمينToolStripMenuItem.Click += new System.EventHandler(this.اداروالعملاءوالمستخدمينToolStripMenuItem_Click);
            // 
            // ادارةالعملاءToolStripMenuItem
            // 
            this.ادارةالعملاءToolStripMenuItem.Name = "ادارةالعملاءToolStripMenuItem";
            this.ادارةالعملاءToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.ادارةالعملاءToolStripMenuItem.Text = "ادارة العملاء";
            this.ادارةالعملاءToolStripMenuItem.Click += new System.EventHandler(this.ادارةالعملاءToolStripMenuItem_Click);
            // 
            // ادارةالمستخدمينToolStripMenuItem
            // 
            this.ادارةالمستخدمينToolStripMenuItem.Name = "ادارةالمستخدمينToolStripMenuItem";
            this.ادارةالمستخدمينToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.ادارةالمستخدمينToolStripMenuItem.Text = "ادارة المستخدمين";
            this.ادارةالمستخدمينToolStripMenuItem.Click += new System.EventHandler(this.ادارةالمستخدمينToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.تسجيلالدخولToolStripMenuItem,
            this.إنشاءحسابToolStripMenuItem,
            this.سحبمالToolStripMenuItem,
            this.ايداعمالToolStripMenuItem,
            this.تحويلمالمنحسابلاخرToolStripMenuItem,
            this.سحبقرضToolStripMenuItem,
            this.اداروالعملاءوالمستخدمينToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(800, 325);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "الواجهة الرئيسية";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem تسجيلالدخولToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تسجيلالدخولToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem حولالبرنامجToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تسجيلالخروجToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem إنشاءحسابToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem فتححسابلشخصواحدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem فتححسابمشتركToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem سحبمالToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ايداعمالToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem تحويلمالمنحسابلاخرToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem سحبقرضToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem اداروالعملاءوالمستخدمينToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ادارةالعملاءToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ادارةالمستخدمينToolStripMenuItem;
    }
}

